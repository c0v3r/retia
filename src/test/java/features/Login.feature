Feature: Login Functionality Feature

As a customer
In order to use the application
I want to login with email and password

  @positiveScenario
  Scenario: Users are able to log in

    Given user navigates to RetiaSoft
    When user logs in
    Then home page should be displayed