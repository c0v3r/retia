package stepDefinitions;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


import io.cucumber.java.en.*;



public class LoginSteps {
    WebDriver driver;

    @Before
    public void before() {
        System.setProperty("webdriver.gecko.driver", "src/resources/geckodriver");
        driver= new FirefoxDriver();
        driver.manage().window().maximize();
    }

    @After
    public void after() {
        driver.quit();
    }

    @Given("user navigates to RetiaSoft")
    public void userNavigatesToRetiaSoft() {
        driver.get("https://cloud.redsanitaria.com/");
    }

    @When("user logs in")
    public void userLogsIn() {
        driver.findElement(By.id("usuario")).sendKeys("morenohernan");
        driver.findElement(By.id("contraseña")).sendKeys("Hernan1");
        driver.findElement(By.cssSelector("button.btn:nth-child(3)")).click();
    }

    @Then("home page should be displayed")
    public void homePageShouldBeDisplayed() {
        String message =  driver.findElement(By.cssSelector("h3.color-black")).getText();

        Assert.assertTrue(message.contains("MORENO , HERNAN"));
    }
}
